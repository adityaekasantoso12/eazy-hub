package com.example.eazyhub.data.source.remote

import com.example.eazyhub.data.source.response.UserDataResponse
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface EazyService {
    @GET("users/v4/profile/{userId}")
    suspend fun getUserData(
        @Path("userId") userId: String, @Header("Authorization") token: String
    ): UserDataResponse
}
