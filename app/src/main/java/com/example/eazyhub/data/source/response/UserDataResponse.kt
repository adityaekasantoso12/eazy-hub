package com.example.eazyhub.data.source.response

import kotlinx.serialization.Serializable

@Serializable
data class UserDataResponse(
    val data: List <UserData>
)