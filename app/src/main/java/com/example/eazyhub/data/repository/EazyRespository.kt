package com.example.eazyhub.data.repository

import android.graphics.Color
import android.util.Log
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.eazyhub.ui.feature.User
import com.example.eazyhub.data.source.remote.EazyApi.retrofitService
import com.example.eazyhub.data.source.response.UserDataResponse
import com.example.eazyhub.data.source.response.UserData
import com.example.eazyhub.data.source.local.UserPreferences
import com.example.eazyhub.data.source.remote.EazyApi
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.flow

class EazyRespository(
    private val dataStore : UserPreferences,
    private val firebaseRef : DatabaseReference
) {
    fun generateQRCodeBitmap(qrValue: String): android.graphics.Bitmap? {
        val hints: MutableMap<EncodeHintType, Any> = hashMapOf()
        hints[EncodeHintType.CHARACTER_SET] = "UTF-8"

        try {
            val writer = QRCodeWriter()
            val bitMatrix: BitMatrix = writer.encode(qrValue, BarcodeFormat.QR_CODE, 512, 512, hints)
            val width = bitMatrix.width
            val height = bitMatrix.height
            val pixels = IntArray(width * height)

            for (y in 0 until height) {
                val offset = y * width
                for (x in 0 until width) {
                    pixels[offset + x] = if (bitMatrix[x, y]) Color.BLACK else Color.WHITE
                }
            }

            val bitmap = android.graphics.Bitmap.createBitmap(width, height, android.graphics.Bitmap.Config.RGB_565)
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
            return bitmap

        } catch (e: WriterException) {
            e.printStackTrace()
        }

        return null
    }
                } else {
                    null
                }
                callback(result)
            }
            override fun onCancelled(error: DatabaseError) {
                callback(null)
            }
        })
    }

    fun getNameFromFirebase(name: String, callback: (String?) -> Unit) {
        val database = FirebaseDatabase.getInstance()
        val myRef: DatabaseReference = database.reference.child("LINK_ACCOUNT").child("ANDROID_TV")

        myRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val userNode = dataSnapshot.child(name)
                val nameValue = userNode.child("name").getValue(String::class.java)
                callback(nameValue)
            }

            override fun onCancelled(error: DatabaseError) {
                callback(null)
            }
        })
    }

    private val _state = MutableStateFlow(
        UserData(
            userId = "",
            fullname = "",
            email = "",
            profile = ""
        )
    )

    suspend fun getUserData(userId: String, token: String): UserDataResponse {
        val data = EazyApi.retrofitService.getUserData(userId, token)
        return data
    }


    fun generateQrToFirebase(): Flow<String> = flow {
        val auth = FirebaseAuth.getInstance()
        val user = auth.currentUser

        if (user != null) {
            val newUser = User(author = user.uid, name = "Aditya Eka Santoso")
            val postValues = newUser.toMap()

            val key = firebaseRef.push().key
            if (key != null) {
                firebaseRef.child(key).setValue(postValues)
            }

            emit(key ?: "")
        }
    }
    suspend fun login(userId: String, token: String) {
        dataStore.setUserId(userId)
        dataStore.setUserToken(token)
        dataStore.setUserLoggedIn(true)
    }
}