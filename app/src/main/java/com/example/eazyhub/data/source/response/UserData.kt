package com.example.eazyhub.data.source.response

import kotlinx.serialization.Serializable

@Serializable
data class UserData(
    val userId: String,
    val fullname: String?,
    val email: String?,
    val profile: String?
)