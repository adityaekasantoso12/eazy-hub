package com.example.eazyhub.data.source.local

import android.content.Context
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

private val Context.dataStore by preferencesDataStore("user_preferences")

class UserPreferences(context: Context) {
    private val dataStore = context.dataStore

    //Cara mendapatkan isUserLogin dari preferences
    val isUserLoggedIn: Flow<Boolean> = dataStore.data
        .map { preferences ->
            preferences[IS_USER_LOGGED_IN_KEY] ?: false
        }

    //PR = Bikin cara mendapatkan userToken dari preferences
    val userToken: Flow<String?> = dataStore.data
        .map { preferences ->
            preferences[USER_TOKEN_KEY]
        }

    //PR = Bikin cara mendapatkan userId dari preferences
    val userId: Flow<String?> = dataStore.data
        .map { preferences ->
            preferences[USER_ID_KEY]
        }

    //Cara menyimpan userLogin ke preferences
    suspend fun setUserLoggedIn(isLoggedIn: Boolean) {
        dataStore.edit { preferences ->
            preferences[IS_USER_LOGGED_IN_KEY] = isLoggedIn
        }
    }
    //Cara menyimpan userToken ke preferences
    suspend fun setUserToken(userToken: String) {
        dataStore.edit { preferences ->
            preferences[USER_TOKEN_KEY] = userToken
        }
    }
    //Cara menyimpan userId ke preferences
    suspend fun setUserId(userId: String) {
        dataStore.edit { preferences ->
            preferences[USER_ID_KEY] = userId
        }
    }

    //Cara menghapus data user
    suspend fun clearUserData() {
        dataStore.edit { preferences ->
            preferences.remove(USER_TOKEN_KEY)
            preferences.remove(USER_ID_KEY)
            preferences[IS_USER_LOGGED_IN_KEY] = false
        }
    }

    companion object {
        private val IS_USER_LOGGED_IN_KEY = booleanPreferencesKey("is_user_logged_in")
        private val USER_TOKEN_KEY = stringPreferencesKey("user_token")
        private val USER_ID_KEY = stringPreferencesKey("user_id")

    }
}