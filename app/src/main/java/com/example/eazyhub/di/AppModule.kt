package com.example.eazyhub.di

import com.example.eazyhub.data.repository.EazyRespository
import com.example.eazyhub.ui.feature.settings.account.AccountViewModel
import com.example.eazyhub.data.source.local.UserPreferences
import com.example.eazyhub.ui.feature.settings.login.LoginViewModel
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single { EazyRespository(get(), get()) }
    single { UserPreferences(get()) }
    single<DatabaseReference>{
        FirebaseDatabase.getInstance()
            .reference.child("LINK_ACCOUNT").child("ANDROID_TV")
    }
    viewModel { AccountViewModel(get(), get(), get()) }
    viewModel { LoginViewModel(get()) }
}