package com.example.eazyhubcompose.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import com.example.eazyhub.R
import com.example.eazyhub.ui.theme.EazyHubComposeTheme
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.Exclude
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.IgnoreExtraProperties
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import android.graphics.Color as AndroidColor

@Composable
fun PopupLoginQR(modifier: Modifier, onClosePopup: () -> Unit, onLoginSuccess: () -> Unit, qrValue: String) {
    Column(
        modifier = Modifier
            .padding(start = 90.dp, end = 90.dp)
            .background(color = androidx.compose.ui.graphics.Color.Black)
    ) {
        Row(
            horizontalArrangement = Arrangement.End,
            modifier = Modifier.fillMaxWidth()
        ) {
            IconButton(
                onClick = onClosePopup,
                modifier = Modifier.padding(15.dp)
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.close),
                    contentDescription = "Close",
                    modifier = Modifier.size(30.dp),
                    tint = androidx.compose.ui.graphics.Color.White
                )
            }
        }
        LazyRow(
            verticalAlignment = Alignment.Top,
            modifier = Modifier.padding(horizontal = 70.dp)
        ) {
            item {
                Column {
                    Text(
                        text = "Cara Login di Eazy di EazyBox:",
                        color = androidx.compose.ui.graphics.Color.White,
                        lineHeight = 1.5.em,
                        style = TextStyle(
                            fontSize = 25.sp,
                            fontWeight = FontWeight.Bold
                        )
                    )
                    Spacer(modifier = Modifier.height(height = 15.dp))
                    Text(
                        text = "1. Masuk menu “Profil” pada aplikasi Eazy di HP Anda\n2. Pilih “Hubungkan ke Android TV”\n3. Arahkan kamera dan pindai QR Code yang ada pada \n    layar TV di samping ini",
                        color = androidx.compose.ui.graphics.Color.White,
                        lineHeight = 2.em,
                        style = TextStyle(fontSize = 15.sp),
                        modifier = Modifier
                            .width(width = 420.dp)
                    )
                    Spacer(modifier = Modifier.height(height = 50.dp))
                }
                Box(
                    modifier = Modifier
                        .width(width = 4.dp)
                        .height(height = 180.dp)
                        .background(color = androidx.compose.ui.graphics.Color.White)
                )
                Spacer(modifier = Modifier.width(width = 40.dp))

                val qrCodeBitmap = generateQRCodeBitmap(qrValue)

                if (qrCodeBitmap != null) {
                    Image(
                        bitmap = qrCodeBitmap.asImageBitmap(),
                        contentDescription = "QR Code",
                        modifier = Modifier.size(size = 180.dp).clickable {
                            onClosePopup()
                            onLoginSuccess()
                        }
                    )
                }
            }
        }
    }
}

//fun getUserIDFromFirebase(): String {
//    val database = FirebaseDatabase.getInstance()
//    val myRef: DatabaseReference = database.reference.child("LINK_ACCOUNT").child("ANDROID_TV")
//    val auth = FirebaseAuth.getInstance()
//    val user = auth.currentUser
//
//    if (user != null) {
//        val newUser = User(author = user.uid, name = "Aditya Eka Santoso")
//        val postValues = newUser.toMap()
//
//        val key = myRef.push().key
//        if (key != null) {
//            myRef.child(key).setValue(postValues)
//        }
//
//        return key ?: ""
//    }
//
//    return ""
//}

fun generateQRCodeBitmap(qrValue: String): android.graphics.Bitmap? {
    val hints: MutableMap<EncodeHintType, Any> = hashMapOf()
    hints[EncodeHintType.CHARACTER_SET] = "UTF-8"

    try {
        val writer = QRCodeWriter()
        val bitMatrix: BitMatrix = writer.encode(qrValue, BarcodeFormat.QR_CODE, 512, 512, hints)
        val width = bitMatrix.width
        val height = bitMatrix.height
        val pixels = IntArray(width * height)

        for (y in 0 until height) {
            val offset = y * width
            for (x in 0 until width) {
                pixels[offset + x] = if (bitMatrix[x, y]) AndroidColor.BLACK else AndroidColor.WHITE
            }
        }

        val bitmap = android.graphics.Bitmap.createBitmap(width, height, android.graphics.Bitmap.Config.RGB_565)
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
        return bitmap

    } catch (e: WriterException) {
        e.printStackTrace()
    }

    return null
}

@IgnoreExtraProperties
data class User(val author: String? = null, val name: String? = null) {
    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "author" to author,
            "name" to name
        )
    }
}

@Preview(showBackground = true, device = Devices.TV_1080p)
@Composable
fun PopupLoginQRPreview() {
    EazyHubComposeTheme {
        PopupLoginQR(modifier = Modifier, onClosePopup = {}, onLoginSuccess = {}, qrValue = "")
    }
}
