package com.example.eazyhub.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.example.eazyhub.R
import com.example.eazyhub.ui.navigation.Screen
import com.example.eazyhub.ui.theme.EazyHubComposeTheme

@Composable
fun Menu(
    navController: NavHostController,
    modifier: Modifier,
    onClick: (String) -> Unit,
    activemenu: String?,
) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Bottom,
    ) {
        Item(
            name = R.string.account,
            navController = navController,
            icon = R.drawable.account,
            route = Screen.Account,
            modifier = if (activemenu == "account") modifier.background(Color(0x80000000), shape = RoundedCornerShape(50)) else modifier,
        )
        Item(
            name = R.string.inbox,
            icon = R.drawable.email,
            route = Screen.Inbox,
            navController = navController,
            modifier = if (activemenu == "inbox") modifier.background(Color(0x80000000), shape = RoundedCornerShape(50)) else modifier,
            )
        Item(
            name = R.string.notifikasi,
            icon = R.drawable.notification,
            route = Screen.Notifikasi,
            navController = navController,
            modifier = if (activemenu == "notifikasi") modifier.background(Color(0x80000000), shape = RoundedCornerShape(50)) else modifier,
            )
        Item(
            name = R.string.internet,
            navController = navController,
            icon = R.drawable.internet,
            route = Screen.Internet,
            modifier = if (activemenu == "internet") modifier.background(Color(0x80000000), shape = RoundedCornerShape(50)) else modifier,
        )
        Item(
            name = R.string.manage,
            icon = R.drawable.manage,
            route = Screen.ManageApp,
            navController = navController,
            modifier = if (activemenu == "manageapp") modifier.background(Color(0x80000000), shape = RoundedCornerShape(50)) else modifier,
            )
        Item(
            name = R.string.screen_size,
            icon = R.drawable.screen_size,
            route = Screen.ScreenSize,
            navController = navController,
            modifier = if (activemenu == "screensize") modifier.background(Color(0x80000000), shape = RoundedCornerShape(50)) else modifier,
            )
        Item(
            name = R.string.android_setting,
            icon = R.drawable.android_setting,
            route = Screen.AndroidSeting,
            navController = navController,
            modifier = if (activemenu == "androidsetting") modifier.background(Color(0x80000000), shape = RoundedCornerShape(50)) else modifier,
            )
        Item(
            name = R.string.help,
            icon = R.drawable.help,
            route = Screen.Help,
            navController = navController,
            modifier = if (activemenu == "help") modifier.background(Color(0x80000000), shape = RoundedCornerShape(50)) else modifier,
            )
    }
}

@Preview(device = Devices.TV_1080p)
@Composable
fun MenuPreview() {
    val navController = rememberNavController()
    EazyHubComposeTheme {
        Menu(navController = navController, modifier = Modifier, activemenu = null, onClick = { route ->
            navController.navigate(route)
        }
        )
    }
}