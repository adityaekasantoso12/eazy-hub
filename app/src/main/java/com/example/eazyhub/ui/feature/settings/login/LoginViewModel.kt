package com.example.eazyhub.ui.feature.settings.login

import androidx.lifecycle.ViewModel
import com.example.eazyhub.data.repository.EazyRespository
import kotlinx.coroutines.flow.Flow

class LoginViewModel(
    private val repository: EazyRespository,

    ): ViewModel() {
    fun generateQrToFirebase(): Flow<String> {
        return repository.generateQrToFirebase()
    }
}