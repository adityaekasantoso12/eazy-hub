package com.example.eazyhub.ui.feature.settings.manageapp

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import com.example.eazyhub.ui.theme.EazyHubComposeTheme

@Composable
fun ManageAppScreen(
    modifier: Modifier = Modifier,
) {
    Column(
        verticalArrangement = Arrangement.spacedBy(5.dp, Alignment.Top),
        modifier = modifier.fillMaxSize()
    ) {
        Text(
            text = "Manage App",
            color = Color.White,
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp,
            modifier = Modifier.padding(top = 20.dp, end = 20.dp, bottom = 20.dp)
        )
        LazyRow(
            horizontalArrangement = Arrangement.spacedBy(20.dp, Alignment.Start),
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .requiredWidth(width = 556.dp)
                .clip(shape = RoundedCornerShape(8.dp))
                .background(color = Color.Black.copy(alpha = 0.5f))
                .padding(all = 16.dp)
        ) {
            item {
                Row(
                    horizontalArrangement = Arrangement.spacedBy(16.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.requiredWidth(width = 255.dp)
                ) {
                    Box(
                        modifier = Modifier
                            .requiredSize(size = 60.dp)
                    ) {
                        Box(
                            modifier = Modifier
                                .fillMaxSize()
                                .clip(shape = CircleShape)
                                .background(color = Color(0xffe3fcef))
                        )
                        Box(
                            modifier = Modifier
                                .fillMaxSize()
                                .clip(shape = CircleShape)
                                .background(color = Color(0xff36b37e))
                        )
                        Text(
                            text = "50%",
                            color = Color.White,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 16.sp,
                                fontWeight = FontWeight.Bold
                            ),
                            modifier = Modifier
                                .align(alignment = Alignment.TopStart)
                                .offset(x = 12.dp, y = 18.dp)
                        )
                    }
                    Column(
                        verticalArrangement = Arrangement.spacedBy(2.dp, Alignment.Top)
                    ) {
                        Text(
                            text = "8.00 GB / 16.00 GB",
                            color = Color.White,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 16.sp,
                                fontWeight = FontWeight.Bold
                            )
                        )
                        Text(
                            text = "Memory Used",
                            color = Color.White,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 14.sp
                            )
                        )
                    }
                }
            }
        }
        Spacer(modifier = Modifier.height(16.dp))
        Column(
            verticalArrangement = Arrangement.spacedBy(15.dp, Alignment.Top),
            modifier = Modifier.requiredWidth(width = 556.dp)
        ) {
            Text(
                text = "Applications List",
                color = Color.White,
                style = TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Bold
                ),
            )
            Column(
                verticalArrangement = Arrangement.spacedBy(12.dp, Alignment.Top),
                modifier = Modifier.fillMaxWidth()
            ) {
                LazyRow(
                    horizontalArrangement = Arrangement.spacedBy(20.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                        .clip(shape = RoundedCornerShape(8.dp))
                        .background(color = Color.Black.copy(alpha = 0.5f))
                        .padding(all = 16.dp)
                ) {
                    item {
                        Box(
                            modifier = Modifier
                                .requiredWidth(width = 120.dp)
                                .requiredHeight(height = 60.dp)
                        ) {
                            Box(
                                modifier = Modifier
                                    .requiredWidth(width = 120.dp)
                                    .requiredHeight(height = 60.dp)
                                    .clip(shape = RoundedCornerShape(10.dp))
                                    .background(color = Color(0xfffe0002))
                            )
                            Image(
                                painter = painterResource(id = R.drawable.youtube),
                                contentDescription = null,
                                modifier = Modifier
                                    .align(alignment = Alignment.TopStart)
                                    .offset(x = 40.dp, y = 10.dp)
                                    .requiredSize(size = 40.dp)
                            )
                        }
                    }
                    item {
                        Text(
                            text = "Youtube",
                            color = Color.White,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 16.sp,
                                fontWeight = FontWeight.Bold
                            ),
                        )
                    }
                    item {
                        Text(
                            text = "                              217.7 MB  |  12/03/2022",
                            color = Color.White,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 15.sp,
                            ),
                        )
                    }
                }
            }
        }
    }
}

@Preview(device = Devices.TV_720p)
@Composable
fun ManageAppScreenPreview() {
    EazyHubComposeTheme {
        ManageAppScreen()
    }
}