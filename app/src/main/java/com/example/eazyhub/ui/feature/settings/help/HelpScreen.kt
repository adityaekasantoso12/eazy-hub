package com.example.eazyhub.ui.feature.settings.help

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp

@Composable
fun HelpScreen(
    modifier: Modifier = Modifier,
) {
    Column(
        modifier = modifier.fillMaxSize()
    ) {
        Text(
            text = "Help",
            color = Color.White,
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp,
            modifier = Modifier.padding(top = 20.dp, end = 20.dp, bottom = 20.dp)
        )
        Column(
            verticalArrangement = Arrangement.spacedBy(20.dp, Alignment.Top),
            modifier = modifier
                .requiredWidth(width = 556.dp)
        ) {
            LazyRow(
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxWidth()
                    .clip(shape = RoundedCornerShape(10.dp))
                    .background(color = Color.White)
                    .padding(all = 16.dp)
            ) {
                item {
                    Row(
                        horizontalArrangement = Arrangement.spacedBy(20.dp, Alignment.Start),
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier
                            .requiredWidth(width = 365.dp)
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.livechat),
                            contentDescription = "live-chat 1",
                            modifier = Modifier
                                .requiredSize(size = 64.dp))
                        Column(
                            verticalArrangement = Arrangement.spacedBy(4.dp, Alignment.Top),
                            modifier = Modifier
                                .fillMaxWidth()
                        ) {
                            Text(
                                text = "What do you need help with?",
                                color = Color(0xff0a0a0a),
                                lineHeight = 1.5.em,
                                style = TextStyle(
                                    fontSize = 20.sp,
                                    fontWeight = FontWeight.Bold),
                                modifier = Modifier
                                    .fillMaxWidth())
                            Text(
                                text = "Scan QR Code beside to contact out Call Center",
                                color = Color(0xff0a0a0a),
                                lineHeight = 1.5.em,
                                style = TextStyle(
                                    fontSize = 16.sp),
                                modifier = Modifier
                                    .fillMaxWidth())
                        }
                    }
                }
                item {
                    Box(
                        modifier = Modifier
                            .requiredSize(size = 100.dp)
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.qr_login),
                            contentDescription = "qr-code-logo-27ADB92152-seeklogo 1",
                            modifier = Modifier
                                .requiredSize(size = 100.dp))
                    }
                }
            }
            Column(
                verticalArrangement = Arrangement.spacedBy(20.dp, Alignment.CenterVertically),
                modifier = Modifier
                    .fillMaxWidth()
                    .clip(shape = RoundedCornerShape(10.dp))
                    .background(color = Color.Black.copy(alpha = 0.5f))
                    .padding(all = 20.dp)
            ) {
                Row(
                    horizontalArrangement = Arrangement.spacedBy(20.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.telephone),
                        contentDescription = "fluent:call-32-regular",
                        modifier = Modifier
                            .requiredSize(size = 24.dp))
                    Text(
                        text = "Call Center",
                        color = Color.White,
                        lineHeight = 1.5.em,
                        style = TextStyle(
                            fontSize = 20.sp),
                        modifier = Modifier
                            .requiredWidth(width = 200.dp))
                    Text(
                        text = ": 021-8469255",
                        color = Color.White,
                        lineHeight = 1.5.em,
                        style = TextStyle(
                            fontSize = 20.sp,
                            fontWeight = FontWeight.Bold),
                        modifier = Modifier
                            .fillMaxWidth())
                }
                Row(
                    horizontalArrangement = Arrangement.spacedBy(20.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.fb),
                        contentDescription = "tabler:brand-facebook",
                        modifier = Modifier
                            .requiredSize(size = 24.dp))
                    Text(
                        text = "Facebook",
                        color = Color.White,
                        lineHeight = 1.5.em,
                        style = TextStyle(
                            fontSize = 20.sp),
                        modifier = Modifier
                            .requiredWidth(width = 200.dp))
                    Text(
                        text = ": Eazy",
                        color = Color.White,
                        lineHeight = 1.5.em,
                        style = TextStyle(
                            fontSize = 20.sp,
                            fontWeight = FontWeight.Bold),
                        modifier = Modifier
                            .fillMaxWidth())
                }
                Row(
                    horizontalArrangement = Arrangement.spacedBy(20.dp, Alignment.Start),
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ig),
                        contentDescription = "mdi:instagram",
                        colorFilter = ColorFilter.tint(Color.White),
                        modifier = Modifier
                            .requiredSize(size = 24.dp))
                    Text(
                        text = "Instagram",
                        color = Color.White,
                        lineHeight = 1.5.em,
                        style = TextStyle(
                            fontSize = 20.sp),
                        modifier = Modifier
                            .requiredWidth(width = 200.dp))
                    Text(
                        text = ": Eazy",
                        color = Color.White,
                        lineHeight = 1.5.em,
                        style = TextStyle(
                            fontSize = 20.sp,
                            fontWeight = FontWeight.Bold),
                        modifier = Modifier
                            .fillMaxWidth())
                }
            }
        }
    }
}

@Preview(device = Devices.TV_720p)
@Composable
fun HelpScreenPreview() {
    HelpScreen()
}
