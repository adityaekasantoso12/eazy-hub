package com.example.eazyhub.ui.navigation

sealed class Screen(val route: String) {
    object Login : Screen("login")
    object Account : Screen("account")
    object Inbox : Screen("inbox")
    object Notifikasi : Screen("notifikasi")
    object Internet : Screen("internet")
    object ManageApp : Screen("manageapp")
    object ScreenSize : Screen("screensize")
    object AndroidSeting : Screen("androidsetting")
    object Help : Screen("help")

}