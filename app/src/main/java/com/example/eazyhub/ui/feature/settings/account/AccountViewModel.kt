package com.example.eazyhub.ui.feature.settings.account

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eazyhub.data.repository.EazyRespository
import com.example.eazyhub.data.source.local.UserPreferences
import com.example.eazyhub.data.source.response.UserDataResponse
import com.example.eazyhub.data.source.response.UserData
import com.example.eazyhub.ui.feature.util.UiState
import com.google.firebase.database.DatabaseReference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

class AccountViewModel(
    private val repository: EazyRespository,
    private val userPreferences: UserPreferences,
    private val firebaseRef : DatabaseReference

) : ViewModel() {
    private val _uiState: MutableStateFlow<UiState<UserDataResponse>> =
        MutableStateFlow(UiState.Loading)
    val uiState: StateFlow<UiState<UserDataResponse>>
        get() = _uiState

    private val _state = MutableStateFlow(
        UserData(
            userId = "",
            fullname = "",
            email = "",
            profile = ""
        )
    )
    val state = _state.asStateFlow()
    fun getUserIdFromFirebase(): Flow<String> {
        return flow {
            repository.getUserIdFromFirebase()
        }
    }
    fun getNameFromFirebase(): Flow<String> {
        return flow {
            repository.getNameFromFirebase()
        }
    }

     fun getUserData(): Flow<String> {
        return repository.getUserData()
    }

    fun generateQR(): Flow<String>  {
       return repository.generateQrToFirebase()
    }
    fun SignOut() {
        viewModelScope.launch(Dispatchers.IO) {
            userPreferences.clearUserData()
        }
    }
}