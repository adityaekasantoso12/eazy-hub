package com.example.eazyhub.ui.feature.settings.notifikasi

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import com.example.eazyhub.ui.theme.EazyHubComposeTheme

@Composable
fun NotifikasiScreen(
    modifier: Modifier = Modifier,
) {
    Column(
        modifier = modifier.fillMaxSize()
    ) {
        Text(
            text = "Notifikasi",
            color = Color.White,
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp,
            modifier = Modifier.padding(top = 20.dp, end = 20.dp, bottom = 20.dp)
        )
        LazyRow(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 20.dp, top = 13.dp, end = 20.dp, bottom = 13.dp)
        ) {
            item {
                Box(
                    modifier = Modifier
                        .width(550.dp)
                        .clip(RoundedCornerShape(10.dp))
                        .background(Color.White)
                        .padding(16.dp),
                ) {
                    Column(
                        verticalArrangement = Arrangement.spacedBy(8.dp),
                    ) {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween,
                        ) {
                            Row(
                                verticalAlignment = Alignment.CenterVertically,
                            ) {
                                Text(
                                    text = "Kamar Utama",
                                    color = Color.Black,
                                    style = TextStyle(
                                        fontSize = 17.sp,
                                        fontWeight = FontWeight.Bold
                                    ),
                                )
                                Text(
                                    text = "New!",
                                    color = Color.Red,
                                    lineHeight = 1.5.em,
                                    style = TextStyle(
                                        fontSize = 17.sp,
                                        fontWeight = FontWeight.SemiBold
                                    ),
                                    modifier = Modifier.padding(start = 8.dp)
                                )
                            }
                            Text(
                                text = "22/03/2023 - 14:00",
                                color = Color.Black,
                                textAlign = TextAlign.End,
                                lineHeight = 1.5.em,
                                style = TextStyle(
                                    fontSize = 15.sp,
                                )
                            )
                        }
                        Text(
                            text = "Perangkat baru berhasil ditautkan",
                            color = Color.Black,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 16.sp,
                            ),
                            modifier = Modifier.fillMaxWidth()
                        )
                    }
                }
            }
        }
        LazyRow(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 20.dp, top = 13.dp, end = 20.dp, bottom = 13.dp)
        ) {
            item {
                Box(
                    modifier = Modifier
                        .width(550.dp)
                        .clip(RoundedCornerShape(10.dp))
                        .background(Color(0x80000000))
                        .padding(16.dp),
                ) {
                    Column(
                        verticalArrangement = Arrangement.spacedBy(8.dp),
                    ) {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween,
                        ) {
                            Row(
                                verticalAlignment = Alignment.CenterVertically,
                            ) {
                                Text(
                                    text = "Pintu Kamar",
                                    color = Color.White,
                                    style = TextStyle(
                                        fontSize = 17.sp,
                                        fontWeight = FontWeight.Bold
                                    ),
                                )
                                Text(
                                    text = "New!",
                                    color = Color.Red,
                                    lineHeight = 1.5.em,
                                    style = TextStyle(
                                        fontSize = 17.sp,
                                        fontWeight = FontWeight.SemiBold
                                    ),
                                    modifier = Modifier.padding(start = 8.dp)
                                )
                            }
                            Text(
                                text = "22/03/2023 - 14:00",
                                color = Color.White,
                                textAlign = TextAlign.End,
                                lineHeight = 1.5.em,
                                style = TextStyle(
                                    fontSize = 15.sp,
                                )
                            )
                        }
                        Text(
                            text = "Pintu kamar berhasil diakses dengan Fingerprint ID #221251",
                            color = Color.White,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 16.sp,
                            ),
                            modifier = Modifier.fillMaxWidth()
                        )
                    }
                }
            }
        }
        LazyRow(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 20.dp, top = 13.dp, end = 20.dp, bottom = 13.dp)
        ) {
            item {
                Box(
                    modifier = Modifier
                        .width(550.dp)
                        .clip(RoundedCornerShape(10.dp))
                        .background(Color(0x80000000))
                        .padding(16.dp),
                ) {
                    Column(
                        verticalArrangement = Arrangement.spacedBy(8.dp),
                    ) {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween,
                        ) {
                            Row(
                                verticalAlignment = Alignment.CenterVertically,
                            ) {
                                Text(
                                    text = "Ruang Tamu",
                                    color = Color.White,
                                    style = TextStyle(
                                        fontSize = 17.sp,
                                        fontWeight = FontWeight.Bold
                                    ),
                                )
                            }
                            Text(
                                text = "22/03/2023 - 14:00",
                                color = Color.White,
                                textAlign = TextAlign.End,
                                lineHeight = 1.5.em,
                                style = TextStyle(
                                    fontSize = 15.sp,
                                )
                            )
                        }
                        Text(
                            text = "Perangkat kamera telah dinonaktifkan",
                            color = Color.White,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 16.sp,
                            ),
                            modifier = Modifier.fillMaxWidth()
                        )
                    }
                }
            }
        }
    }
}


@Preview(device = Devices.TV_720p)
@Composable
fun AccountScreenPreview() {
    EazyHubComposeTheme {
        NotifikasiScreen()
    }
}