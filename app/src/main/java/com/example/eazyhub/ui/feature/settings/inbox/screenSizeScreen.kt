package com.example.eazyhub.ui.feature.settings.inbox

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import com.example.eazyhub.ui.theme.EazyHubComposeTheme

@Composable
fun InboxScreen(
    modifier: Modifier = Modifier,
) {
    Column(
        modifier = modifier.fillMaxSize()
    ) {
        Text(
            text = "Inbox",
            color = Color.White,
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp,
            modifier = Modifier.padding(top = 20.dp, end = 20.dp, bottom = 20.dp)
        )
        LazyRow(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 20.dp, top = 13.dp, end = 20.dp, bottom = 13.dp)
        ) {
            item {
                Box(
                    modifier = Modifier
                        .width(550.dp)
                        .clip(RoundedCornerShape(10.dp))
                        .background(Color.White)
                        .padding(16.dp),
                ) {
                    Column(
                        verticalArrangement = Arrangement.spacedBy(8.dp),
                    ) {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween,
                        ) {
                            Row(
                                verticalAlignment = Alignment.CenterVertically,
                            ) {
                                Text(
                                    text = "Eazy System",
                                    color = Color.Black,
                                    style = TextStyle(
                                        fontSize = 17.sp,
                                        fontWeight = FontWeight.Bold
                                    ),
                                )
                                Text(
                                    text = "New!",
                                    color = Color.Red,
                                    lineHeight = 1.5.em,
                                    style = TextStyle(
                                        fontSize = 17.sp,
                                        fontWeight = FontWeight.SemiBold
                                    ),
                                    modifier = Modifier.padding(start = 8.dp)
                                )
                            }
                            Text(
                                text = "22/03/2023 - 14:00",
                                color = Color.Black,
                                textAlign = TextAlign.End,
                                lineHeight = 1.5.em,
                                style = TextStyle(
                                    fontSize = 15.sp,
                                )
                            )
                        }
                        Text(
                            text = "Informasi Update Perangkat Lunak",
                            color = Color.Black,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 16.sp,
                                fontWeight = FontWeight.SemiBold
                            ),
                            modifier = Modifier.fillMaxWidth()
                        )
                        Text(
                            text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tempus gravida turpis in semper. Quisque dolor quam, vulputate sed vestibulum nec, varius ut massa. Suspendisse congue risus nec sodales vulputate.",
                            color = Color.Black,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 15.sp
                            ),
                            modifier = Modifier.fillMaxWidth()
                        )
                    }
                }
            }
        }
        LazyRow(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 20.dp, top = 13.dp, end = 20.dp, bottom = 13.dp)
        ) {
            item {
                Box(
                    modifier = Modifier
                        .width(550.dp)
                        .clip(RoundedCornerShape(10.dp))
                        .background(Color(0x80000000))
                        .padding(16.dp),
                ) {
                    Column(
                        verticalArrangement = Arrangement.spacedBy(8.dp),
                    ) {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween,
                        ) {
                            Text(
                                text = "Eazy System",
                                color = Color.White,
                                lineHeight = 1.5.em,
                                style = TextStyle(
                                    fontSize = 17.sp,
                                    fontWeight = FontWeight.SemiBold
                                ),
                                modifier = Modifier.weight(1f)
                            )
                            Text(
                                text = "22/03/2023 - 14:00",
                                color = Color.White,
                                textAlign = TextAlign.End,
                                lineHeight = 1.5.em,
                                style = TextStyle(
                                    fontSize = 15.sp,
                                )
                            )
                        }
                        Text(
                            text = "Informasi Update Perangkat Lunak",
                            color = Color.White,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 16.sp,
                                fontWeight = FontWeight.SemiBold
                            ),
                            modifier = Modifier.fillMaxWidth()
                        )
                        Text(
                            text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tempus gravida turpis in semper. Quisque dolor quam, vulputate sed vestibulum nec, varius ut massa. Suspendisse congue risus nec sodales vulputate.",
                            color = Color.White,
                            lineHeight = 1.5.em,
                            style = TextStyle(
                                fontSize = 15.sp
                            ),
                            modifier = Modifier.fillMaxWidth()
                        )
                    }
                }
            }
        }
    }
}


@Preview(device = Devices.TV_720p)
@Composable
fun AccountScreenPreview() {
    EazyHubComposeTheme {
        InboxScreen()
    }
}